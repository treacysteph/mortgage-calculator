import testStoreState from '../tests/testStoreState';

export const overrideTestStoreState = (reducerOverride) => {
  const combinedReducer = {};
  Object.keys(testStoreState).forEach((reducer) => {
    combinedReducer[reducer] = reducer in reducerOverride
      ? Object.assign({}, testStoreState[reducer], reducerOverride[reducer])
      : testStoreState[reducer];
  });
  return combinedReducer;
};
