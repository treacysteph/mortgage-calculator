export const SWITCH_THEME = 'SWITCH_THEME';
export const PING_BACKEND = 'PING_BACKEND';
export const PING_BACKEND_COMPLETE = 'PING_BACKEND_COMPLETE';

const initialState = {
  theme: 'dark',
  backendTestResponse: undefined,
  backendTestTime: undefined
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SWITCH_THEME:
      const { theme } = state;
      return { ...state, theme: theme === 'light' ? 'dark' : 'light' };
    case PING_BACKEND_COMPLETE:
      return {
        ...state,
        backendTestResponse: action.responseData,
        backendTestTime: action.responseTime
      };
    default:
      return state;
  }
};

export const switchTheme = () => ({
  type: SWITCH_THEME
});

/* Boilerplate code below this line */
export const pingBackend = () => ({
  type: PING_BACKEND
});

export const pingBackendComplete = (responseData, responseTime) => ({
  type: PING_BACKEND_COMPLETE,
  responseData,
  responseTime
});
