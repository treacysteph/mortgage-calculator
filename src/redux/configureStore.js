import { combineReducers, createStore, applyMiddleware } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import settingsReducer from './ducks/settings';
import { watcherSaga } from '../sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();
const loggerMiddleware = createLogger();

let middleware = [sagaMiddleware];
if (process.env.NODE_ENV !== 'production') {
  middleware = [...middleware, loggerMiddleware];
}

const reducer = combineReducers({
  settings: settingsReducer
});

const store = createStore(
  reducer,
  {},
  applyMiddleware(...middleware)
);

sagaMiddleware.run(watcherSaga);

export default store;
