import light from '../themes/light';
import dark from '../themes/dark';

const BACKEND_PROD_URL = 'http://localhost:8081';
const BACKEND_DEV_URL = 'http://localhost:8081';

export const backendBaseUrl = process.env.NODE_ENV === 'production' ? BACKEND_PROD_URL : BACKEND_DEV_URL;
export const availableThemes = { light, dark };

export const mortgageInputs = [
  {
    label: 'Mortgage Amount',
    dataTest: 'mortgage-amount'
  },
  {
    label: 'Interest Rate',
    dataTest: 'interest-rate'
  },
  {
    label: 'Amortization Period',
    dataTest: 'amortization-period'
  },
  {
    label: 'Payment Frequency',
    dataTest: 'payment-frequency'
  },
  {
    label: 'Term',
    dataTest: 'term'
  }
];
