import React from 'react';
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';
import configureMockStore from 'redux-mock-store';
import { object } from 'prop-types';
import en from '../messages/en';
import { availableThemes } from '../constants/constants';
import testStoreState from './testStoreState';
import { overrideTestStoreState } from '../utils/utils';

const TestComponentWrapper = (props) => {
  const theme = 'dark';
  const mockStore = configureMockStore();
  const { children, reducerOverride = {} } = props;
  const overrideReducersCount = Object.keys(reducerOverride).length;
  const testState = overrideReducersCount > 0
    ? overrideTestStoreState(reducerOverride)
    : testStoreState;

  const testStore = mockStore(testState);

  return (
    <Provider store={testStore}>
      <IntlProvider locale="en" messages={en}>
        <ThemeProvider theme={createMuiTheme({ ...availableThemes[theme], props: { MuiWithWidth: { initialWidth: 'lg' } } })}>
          {children}
        </ThemeProvider>
      </IntlProvider>
    </Provider>
  );
};

TestComponentWrapper.propTypes = {
  children: object,
  reducerOverride: object
};

export default TestComponentWrapper;
