
const testStoreState = {
  settings: {
    theme: 'dark',
    backendTestResponse: undefined,
    backendTestTime: undefined
  }
};

export default testStoreState;
