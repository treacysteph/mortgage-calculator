import React from 'react';
import { mount } from 'enzyme';
import CashFlowCalculator from '../../Components/CashflowCalculator';
import TestComponentWrapper from '../TestComponentWrapper';

describe('Cash flow calculator', () => {
  const getWrapper = () => {
    return mount(
      <TestComponentWrapper>
        <CashFlowCalculator />
      </TestComponentWrapper>
    );
  };

  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });

});