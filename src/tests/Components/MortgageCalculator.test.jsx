import React from 'react';
import { mount } from 'enzyme';
import Mortgage from '../../Components/MortgageCalculator';
import TestComponentWrapper from '../TestComponentWrapper';
import { mortgageInputs } from '../../constants/constants';

describe('Mortgage Calculator', () => {
  const getWrapper = () => {
    return mount(
      <TestComponentWrapper>
        <Mortgage />
      </TestComponentWrapper>
    );
  };

  it('should render', () => {
    const wrapper = getWrapper();
    expect(wrapper).toBeDefined();
  });

  it('should contain a Mortgage Amount text field', () => {
    const wrapper = getWrapper();
    const dataTest = `[data-test="mortgage-amount"]`;
    const mortgageAmountTextArea = wrapper.find(dataTest).hostNodes();
    expect(mortgageAmountTextArea).toBeDefined();
  });

  it('should contain a Interest Rate text field', () => {
    const wrapper = getWrapper();
    const dataTest = `[data-test="interest-rate"]`;
    const mortgageAmountTextArea = wrapper.find(dataTest).hostNodes();
    expect(mortgageAmountTextArea).toBeDefined();
  });

  it('should contain a Amortization Period text field', () => {
    const wrapper = getWrapper();
    const dataTest = '[data-test="amortization-period"]';
    const mortgageAmountTextArea = wrapper.find(dataTest).hostNodes();
    expect(mortgageAmountTextArea).toBeDefined();
  });

  it('should contain a Payment Frequency text field', () => {
    const wrapper = getWrapper();
    const dataTest = `[data-test="payment-frequency"]`;
    const mortgageAmountTextArea = wrapper.find(dataTest).hostNodes();
    expect(mortgageAmountTextArea).toBeDefined();
  });

  it('should contain a Term text field', () => {
    const wrapper = getWrapper();
    const dataTest = `[data-test="term"]`;
    const mortgageAmountTextArea = wrapper.find(dataTest).hostNodes();
    expect(mortgageAmountTextArea).toBeDefined();
  });

});