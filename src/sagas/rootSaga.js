import { takeLatest, call, put } from 'redux-saga/effects';
import axios from 'axios';
import { PING_BACKEND, PING_BACKEND_COMPLETE } from '../redux/ducks/settings';
import { backendBaseUrl } from '../constants/constants';

// watcher saga: watches for actions dispatched to the store, starts worker saga
export function* watcherSaga() {
  yield takeLatest(PING_BACKEND, pingBackend);
}

// function that makes the api request and returns a Promise for response
function doPingBackend() {
  return axios.request({
    method: 'get',
    url: `${backendBaseUrl}/test`,
  });
}

// worker saga: makes the api call when watcher saga sees the action
function* pingBackend() {
  let responseData = '';
  let responseTime;
  try {
    const response = yield call(doPingBackend);
    const { status, statusText, headers } = response;
    responseTime = headers['x-response-time'];
    responseData = `${status}: ${statusText}`;
  } catch (error) {
    responseData = error.message;
  }
  yield put({ type: PING_BACKEND_COMPLETE, responseData, responseTime });
}
