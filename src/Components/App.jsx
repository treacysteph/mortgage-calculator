import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import { Grid } from '@material-ui/core';
import Header from './Header/Header';
import Footer from './Footer/Footer';
import NotFound from './NotFound';
import MortgageCalculator from './MortgageCalculator';
import CashFlowCalculator from './CashflowCalculator';

const useStyles = makeStyles(() => ({
  root: {
    minHeight: '100vh'
  },
  header: {
    width: '100%'
  },
  footer: {
    marginTop: 'auto',
    padding: '10px'
  }
}));

const App = (props) => {
  const classes = useStyles(props);
  return (
    <Grid container direction="column" className={classes.root}>
      <Grid item className={classes.header}>
        <Header />
      </Grid>
      <Grid item className={classes.content}>
        <Switch>
          <Route exact path="/" render={() => <MortgageCalculator />} />
          <Route exact path="/cashflow" render={() => <CashFlowCalculator />} />
          <Route render={() => <NotFound />} />
        </Switch>
      </Grid>
      <div className={classes.footer}>
        <Grid item>
          <Footer />
        </Grid>
      </div>
    </Grid>
  );
};

export default App;
