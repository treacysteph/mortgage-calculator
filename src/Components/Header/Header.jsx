import React, { useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { AppBar, Tab, Tabs, Link } from '@material-ui/core';
import { withRouter } from 'react-router-dom';

const Header = () => {
  const [tabValue, setTabValue] = useState(0);

  const handleChange = (__, newTabValue) => {
    setTabValue(newTabValue);
  };


  return (
    <AppBar position="static">
      <Tabs
        value={tabValue}
        onChange={handleChange}
      >
        <Tab label={<FormattedMessage id="mortgage_calculator" />} component={Link} to="/" />
        <Tab label={<FormattedMessage id="cashflow_calculator" />} component={Link} to="/cashflow" />
      </Tabs>

    </AppBar>
  );
};

export default withRouter(Header);
