import React from 'react';
import { Typography, Grid, Button } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { pingBackend } from '../redux/ducks/settings';

// Boilerplate component
const BackendTest = () => {
  const dispatch = useDispatch();
  const response = useSelector(state => state.settings.backendTestResponse);
  const responseTime = useSelector(state => state.settings.backendTestTime);

  return (
    <Grid container direction="column">
      <Grid item container justify="center">
        <Button variant="outlined" onClick={() => dispatch(pingBackend())}>
          <FormattedMessage id="home_button_ping_backend" />
        </Button>
      </Grid>
      {response && responseTime && (
        <>
          <Grid item container justify="center">
            <Typography variant="subtitle2">
              {response}
            </Typography>
          </Grid>
          <Grid item container justify="center">
            <Typography variant="subtitle2">
              {responseTime}
            </Typography>
          </Grid>
        </>
      )}
    </Grid>
  );
};

export default BackendTest;
