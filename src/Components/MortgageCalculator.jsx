import React from 'react';
import { Grid, TextField } from '@material-ui/core';
import { mortgageInputs } from '../constants/constants';

const Mortgage = () => (
  <Grid container justify="center">
    {mortgageInputs.map(input => (
      <TextField
        data-test={input.dataTest}
        label={input.label}
        variant="outlined"
        key={input.label}
      />
    ))}
  </Grid>
);

export default Mortgage;
